from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Conv2D, MaxPooling2D, Flatten, Dense, Dropout

def cnn_model():
    model = Sequential()
    # 3 identical convolutional layers without activation function
    model.add(Conv2D(32, (3, 3), padding='same', input_shape=(500, 500, 3), activation='relu'))
    # model.add(Conv2D(32, (3, 3), padding='same'))
    # model.add(Conv2D(32, (3, 3), padding='same', activation='relu'))
    # Convolutional and maxpooling layers four times
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Conv2D(64, (3, 3), padding='same', activation='relu'))
    #model.add(Conv2D(64, (3, 3), padding='same', activation='relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Conv2D(128, (3, 3), padding='same', activation='relu'))
    #model.add(Conv2D(128, (3, 3), padding='same', activation='relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Conv2D(256, (3, 3), padding='same', activation='relu'))
    #model.add(Conv2D(256, (3, 3), padding='same', activation='relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Conv2D(512, (3, 3), padding='same', activation='relu'))
    #model.add(Conv2D(512, (3, 3), padding='same', activation='relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    # model.add(Conv2D(1024, (3, 3), padding='same', activation='relu'))
    # model.add(Conv2D(1024, (3, 3), padding='same', activation='relu'))
    # model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Flatten())
    # model.add(Dense(1024, activation='relu'))
    # model.add(Dropout(0.5))
    # model.add(Dense(512, activation='relu'))
    # model.add(Dropout(0.5))
    # model.add(Dense(256, activation='relu'))
    # model.add(Dropout(0.5))
    # model.add(Dense(128, activation='relu'))
    # model.add(Dropout(0.5))
    model.add(Dense(2, activation='sigmoid'))
    return model

import tensorflow as tf
from tensorflow.keras.layers import Input, Conv2D, MaxPooling2D, Flatten, Dense

def new_cnn_model():
    # Define the input shape
    input_shape = (500, 500, 3)

    # Define the input layer
    inputs = Input(shape=input_shape)

    # Define the convolutional layers
    conv1 = Conv2D(32, kernel_size=(3, 3), activation='relu')(inputs)
    conv2 = Conv2D(64, kernel_size=(3, 3), activation='relu')(conv1)
    conv3 = Conv2D(128, kernel_size=(3, 3), activation='relu')(conv2)

    # Define the pooling layers
    pool1 = MaxPooling2D(pool_size=(2, 2))(conv3)
    pool2 = MaxPooling2D(pool_size=(2, 2))(pool1)

    # Flatten the output from the last pooling layer
    flatten = Flatten()(pool2)

    # Define the fully connected layers
    dense1 = Dense(512, activation='relu')(flatten)
    #dense2 = Dense(256, activation='relu')(dense1)

    # Define the output layer with 2 units (x and y coordinates)
    outputs = Dense(2, activation='sigmoid')(dense1)
    #outputs = Dense(2, activation='sigmoid')(dense2)
    model = tf.keras.Model(inputs=inputs, outputs=outputs)
    return model
