import os
import numpy as np
from PIL import Image
import random 
from sklearn.preprocessing import MinMaxScaler
import pickle as pkl
from tqdm import tqdm

def load_data(image_folder_dummy, image_folder_redfe, label_folder_dummy, label_folder_redfe, train_split=0.8, shuffle=True):
    
    # Load image filenames
    image_filenames = os.listdir(image_folder_dummy) + os.listdir(image_folder_redfe)
    # Load label filenames
    label_filenames = os.listdir(label_folder_dummy) + os.listdir(label_folder_redfe)
    # Sort filenames to ensure correspondence between images and labels
    image_filenames.sort()
    label_filenames.sort()
    # Load images and labels as arrays
    images = []
    labels = []

    # Loop over all image and label filenames
    for image_filename, label_filename in tqdm(zip(image_filenames, label_filenames),ncols=100, total=len(image_filenames)):
    #for image_filename, label_filename in zip(image_filenames, label_filenames):
        # x = random.random()
        # if(x < 0.25):
        if('dummy' in image_filename):
            image_path = os.path.join(image_folder_dummy, image_filename)
            label_path = os.path.join(label_folder_dummy, label_filename)
        elif('redfe' in image_filename):
            image_path = os.path.join(image_folder_redfe, image_filename)
            label_path = os.path.join(label_folder_redfe, label_filename)
        image = np.array(Image.open(image_path), dtype=np.float32)
        #image = np.expand_dims(image, axis=0)
        label = np.load(label_path).reshape(2)
        #label = np.expand_dims(label, axis=0)
        images.append(image)
        labels.append(label)
        
    images = np.array(images)
    labels = np.array(labels)
    # Convert lists to arrays
    print(f"{images.shape=}")
    print(f"{labels.shape=}")
    input_shape = images.shape
    images = images.reshape((-1, 3))
    # Create scaler object and fit to data
    scaler_images = MinMaxScaler()
    scaler_images.fit(images)
    # Transform data using scaler
    images = scaler_images.transform(images)
    images = images.reshape(input_shape)
    # scaler_labels = MinMaxScaler()
    # labels = scaler_labels.fit_transform(labels)
    
    # Shuffle data
    if shuffle:
        indices = np.arange(len(images))
        np.random.shuffle(indices)
        images = images[indices]
        labels = labels[indices]
    
    # Split data into training and validation sets
    split_index = int(train_split * len(images))
    train_images = images[:split_index]
    train_labels = labels[:split_index]
    val_images = images[split_index:]
    val_labels = labels[split_index:]
    np.save('/mnt/project_mnt/atlas/atlas_gen_fs/vannolil/triplets_cnn/data/train_images.npy', train_images)
    np.save('/mnt/project_mnt/atlas/atlas_gen_fs/vannolil/triplets_cnn/data/train_labels.npy', train_labels)
    np.save('/mnt/project_mnt/atlas/atlas_gen_fs/vannolil/triplets_cnn/data/val_images.npy', val_images)
    np.save('/mnt/project_mnt/atlas/atlas_gen_fs/vannolil/triplets_cnn/data/val_labels.npy', val_labels)
    #return (train_images, train_labels), (val_images, val_labels)
    with open('/mnt/project_mnt/atlas/atlas_gen_fs/vannolil/triplets_cnn/data/image_scaler.pkl', 'wb') as f:
        pkl.dump(scaler_images, f)
    # with open('/mnt/project_mnt/atlas/atlas_gen_fs/vannolil/triplets_cnn/data/label_scaler.pkl', 'wb') as f:
    #     pkl.dump(scaler_labels, f)

if __name__ == '__main__':
    image_folder_dummy = "/mnt/project_mnt/atlas/atlas_gen_fs/vannolil/triplets_cnn/data/dummies_onlyrot"
    label_folder_dummy = "/mnt/project_mnt/atlas/atlas_gen_fs/vannolil/triplets_cnn/data/dummies_label_onlyrot"
    image_folder_redfe = "/mnt/project_mnt/atlas/atlas_gen_fs/vannolil/triplets_cnn/data/redfe_onlyrot"
    label_folder_redfe = "/mnt/project_mnt/atlas/atlas_gen_fs/vannolil/triplets_cnn/data/redfe_label_onlyrot"
    load_data(image_folder_dummy, image_folder_redfe, label_folder_dummy, label_folder_redfe)
