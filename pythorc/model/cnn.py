import torch
import torch.nn as nn
import torchvision.transforms as transforms
import numpy as np

class CNNModel(nn.Module):
    def __init__(self):
        super(CNNModel, self).__init__()
        self.conv_layers = nn.Sequential(
            nn.Conv2d(3, 32, kernel_size=3, padding=1),
            nn.ReLU(),
            nn.MaxPool2d(kernel_size=2),
            nn.Conv2d(32, 64, kernel_size=3, padding=1),
            nn.ReLU(),
            nn.MaxPool2d(kernel_size=2),
            nn.Conv2d(64, 128, kernel_size=3, padding=1),
            nn.ReLU(),
            nn.MaxPool2d(kernel_size=2),
            nn.Conv2d(128, 256, kernel_size=3, padding=1),
            nn.ReLU(),
            nn.MaxPool2d(kernel_size=2),
            nn.Conv2d(256, 512, kernel_size=3, padding=1),
            nn.ReLU(),
            nn.MaxPool2d(kernel_size=2)
        )
        self.fc_layers = nn.Sequential(
            nn.Flatten(),
            nn.Linear(512*15*15, 2),
            nn.Sigmoid()
        )

    def forward(self, x):
        x = self.conv_layers(x)
        x = self.fc_layers(x)
        return x

model = CNNModel()

if __name__ == '__main__':
    print(model)

    def augment_data(images, labels):
        # Convert numpy arrays to PyTorch tensors
        images = torch.from_numpy(images)
        labels = torch.from_numpy(labels)

        # Define data augmentation transformations
        transform = transforms.Compose([
            transforms.RandomRotation(10),
            transforms.RandomHorizontalFlip(),
            transforms.RandomVerticalFlip(),
            transforms.ColorJitter(brightness=0.2, contrast=0.2, saturation=0.2, hue=0.1),
            transforms.RandomAffine(degrees=0, translate=(0.2, 0.2)),
            transforms.ToTensor()
        ])

        # Apply data augmentation to images
        augmented_images = []
        for image in images:
            augmented_images.append(transform(image))
        augmented_images = torch.stack(augmented_images)

        # Return augmented images and labels as numpy arrays
        return augmented_images.numpy(), labels.numpy()

    # Define loss function
    criterion = nn.MSELoss()

    # Define optimizer
    optimizer = optim.Adam(model.parameters())

    # Split data into train and validation sets
    train_ratio = 0.8
    train_size = int(train_ratio * len(train_imgs))
    train_imgs, train_labels = augment_data(train_imgs[:train_size], train_labels[:train_size])
    val_imgs, val_labels = augment_data(train_imgs[train_size:], train_labels[train_size:])

    # Convert numpy arrays to PyTorch tensors
    train_imgs = torch.tensor(train_imgs, dtype=torch.float32)
    train_labels = torch.tensor(train_labels, dtype=torch.float32)
    val_imgs = torch.tensor(val_imgs, dtype=torch.float32)
    val_labels = torch.tensor(val_labels, dtype=torch.float32)

    # Define batch size and number of epochs
    batch_size = 32
    num_epochs = 10

    # Define lists to store training and validation losses
    train_losses = []
    val_losses = []

    # Train the model
    for epoch in range(num_epochs):
        # Set model to train mode
        model.train()

        # Shuffle the training data
        permutation = torch.randperm(train_imgs.size()[0])

        for i in range(0, train_imgs.size()[0], batch_size):
            # Get a batch of images and labels
            indices = permutation[i:i+batch_size]
            batch_imgs, batch_labels = train_imgs[indices], train_labels[indices]

            # Zero the parameter gradients
            optimizer.zero_grad()

            # Forward pass
            outputs = model(batch_imgs)

            # Calculate loss
            loss = criterion(outputs, batch_labels)

            # Backward pass
            loss.backward()

            # Update weights
            optimizer.step()

        # Evaluate the model on the validation set
        model.eval()
        with torch.no_grad():
            val_outputs = model(val_imgs)
            val_loss = criterion(val_outputs, val_labels)
            val_losses.append(val_loss.item())

        # Print epoch and validation loss
        print(f"Epoch {epoch+1}/{num_epochs}, Validation loss: {val_loss.item():.4f}")

        # Save training and validation losses
        train_losses.append(loss.item())

    torch.save(model.state_dict(), "cnn_model.pt")
    np.savez("losses.npz", train=train_losses, val=val_losses)

