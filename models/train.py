import numpy as np
import tensorflow as tf
from tensorflow.keras.models import load_model
from tensorflow.keras.callbacks import EarlyStopping
from tensorflow.keras.optimizers import Adam
from PIL import Image
import matplotlib.pyplot as plt
from sklearn.preprocessing import StandardScaler, MinMaxScaler
from sklearn.metrics import mean_squared_error

import pickle as pkl
import sys
import os
module_path = os.path.abspath(os.path.join('/mnt/project_mnt/atlas/atlas_gen_fs/vannolil/cnn_triplets/project/'))
if module_path not in sys.path:
    sys.path.append(module_path)
from cnn import cnn_model, new_cnn_model
import argparse

def parseArgs():
    parser = argparse.ArgumentParser(description='Train the CNN model')
    parser.add_argument('-g', '--gpus', help='GPU list to use', type=str, default='0')
    parser.add_argument('-e', '--epochs', help='Number of epochs', type=int, default=30)
    parser.add_argument('-b', '--batch_size', help='Batch size', type=int, default=32)
    parser.add_argument('-s', '--suffix', help='Suffix for the model name', type=str, default='')
    parser.add_argument('-l', '--lr', help='Learning rate', type=float, default=0.001)
    args = parser.parse_args()
    return args

args = parseArgs()
os.environ["CUDA_VISIBLE_DEVICES"] = args.gpus
physical_devices = tf.config.experimental.list_physical_devices('GPU')
print("----------------------------------------------------------")
#print(physical_devices)
for i in range(len(physical_devices)):
    config0 = tf.config.experimental.set_memory_growth(physical_devices[i], True) 
print("----------------------------------------------------------")
print(f'{args=}')
print("----------------------------------------------------------")
suffix = args.suffix
batch_size = args.batch_size
data_folder = "/mnt/project_mnt/atlas/atlas_gen_fs/vannolil/cnn_triplets/data/"
plot_folder = "/mnt/project_mnt/atlas/atlas_gen_fs/vannolil/cnn_triplets/data/plots/"
print(f'loading data...')
img = np.load("/mnt/project_mnt/atlas/atlas_gen_fs/vannolil/cnn_triplets/data/img.npy")
label = np.load("/mnt/project_mnt/atlas/atlas_gen_fs/vannolil/cnn_triplets/data/label.npy")
print(f'loading data done')

print("----------------------------------------------------------")
print(f"{img.shape=}, {label.shape=}")
print(f"{img.max()=}, {img.min()=}")
print(f"{label.max()=}, {label.min()=}")

dataset = tf.data.Dataset.from_tensor_slices((img, label))
dataset = dataset.shuffle(buffer_size=len(img), reshuffle_each_iteration=False)

# Split the dataset into training, validation, and test sets
train_size = int(0.8 * len(img))
val_size = int(0.18 * len(img))
test_size = len(img) - train_size - val_size

train_dataset = dataset.take(train_size)
val_dataset = dataset.skip(train_size).take(val_size)
test_dataset = dataset.skip(train_size + val_size)

test_images = []
test_labels = []
for image, label in test_dataset:
    test_images.append(image.numpy())
    test_labels.append(label.numpy())

# Convert the images and labels to numpy arrays
test_images = np.array(test_images)
test_labels = np.array(test_labels)

# Save the images and labels to separate numpy files
np.save(f'{data_folder}test_images.npy', test_images)
np.save(f'{data_folder}test_labels.npy', test_labels)
print("----------------------------------------------------------")
print(f"test images and labels saved as {data_folder}test_images.npy and {data_folder}test_labels.npy")

# Batch the datasets
train_dataset = train_dataset.batch(batch_size)
val_dataset = val_dataset.batch(batch_size)
print("----------------------------------------------------------")
print(f"{train_dataset.element_spec=}\n{val_dataset.element_spec=}")


if __name__ == "__main__":

    model = cnn_model()
    model.summary()
    optimizer = Adam(learning_rate=args.lr)
    model.compile(loss='mse', optimizer=optimizer, metrics=['mse'])
    # create early stopping callback
    early_stop = EarlyStopping(monitor='val_loss', patience=5)
    history = model.fit(train_dataset, validation_data=val_dataset, batch_size=batch_size, epochs=args.epochs,  callbacks=[early_stop])
    loss, mse = model.evaluate(val_dataset)
    print("----------------------------------------------------------")
    print(f"loss: {loss}, mse: {mse}")
    with open('/mnt/project_mnt/atlas/atlas_gen_fs/vannolil/cnn_triplets/models/history'+suffix+'.pkl', 'wb') as f:
        pkl.dump(history.history, f)
        print("----------------------------------------------------------")
        print(f"Saved history as model/history{suffix}.pkl")
    with open('/mnt/project_mnt/atlas/atlas_gen_fs/vannolil/cnn_triplets/models/loss_history'+suffix+'.pkl', 'wb') as f:
        pkl.dump(history.history['loss'], f)
        pkl.dump(history.history['val_loss'], f)
        print("----------------------------------------------------------")
        print(f"Saved loss history as model/loss_history{suffix}.pkl")
    model.save('/mnt/project_mnt/atlas/atlas_gen_fs/vannolil/cnn_triplets/models/model'+suffix+'.h5')
    print("----------------------------------------------------------")
    print(f"Saved model as model/model{suffix}.h5")