import numpy as np
import tensorflow as tf
from tensorflow.keras.models import load_model
from PIL import Image
import matplotlib.pyplot as plt
from sklearn.preprocessing import StandardScaler, MinMaxScaler
from sklearn.metrics import mean_squared_error
import pickle as pkl
import sys
import os

def rmse_loss(y_true, y_pred):
    return tf.sqrt(tf.reduce_mean(tf.square(y_true - y_pred)))

suffix = '_lr10-5'
data_folder = "/mnt/project_mnt/atlas/atlas_gen_fs/vannolil/cnn_triplets/data/"
plot_folder = "/mnt/project_mnt/atlas/atlas_gen_fs/vannolil/cnn_triplets/data/plots/"

print(f'loading data...')
# Load model
model = load_model(f"/mnt/project_mnt/atlas/atlas_gen_fs/vannolil/cnn_triplets/models/model{suffix}.h5")
image_scaler = pkl.load(open("/mnt/project_mnt/atlas/atlas_gen_fs/vannolil/cnn_triplets/data/image_scaler.pkl", "rb"))
label_scaler = pkl.load(open("/mnt/project_mnt/atlas/atlas_gen_fs/vannolil/cnn_triplets/data/label_scaler.pkl", "rb"))
test_images = np.load(data_folder+"test_images.npy")
test_labels = np.load(data_folder+"test_labels.npy")
print(f'loading data done')


def validation_plots():
    with open(f'/mnt/project_mnt/atlas/atlas_gen_fs/vannolil/cnn_triplets/models/loss_history{suffix}.pkl', 'rb') as f:
        loss = pkl.load(f)
        val = pkl.load(f)
    epochs = range(1, len(loss) + 1)
    fig, ax2 = plt.subplots(figsize=(8,8))
    ax2.plot(epochs, loss, 'r', label='Training loss')
    ax2.plot(epochs, val, 'b', label='Validation loss')
    ax2.set_title('Training and validation RMS loss')
    ax2.set_xlabel('Epochs')
    ax2.set_ylabel('Loss')
    ax2.legend()
    fig.savefig(f'{plot_folder}loss_and_val_plot_model{suffix}.jpg')
    print(f"loss and val plot saved as {plot_folder}loss_and_val_plot_model{suffix}.jpg")
    plt.close()
    # plt.show()

def plot_prediction(prediction, test_labels):
    # Calculate RMSE
    rmse = mean_squared_error(test_labels, prediction, squared=True)
    plt.scatter(test_labels[:,0], test_labels[:,1], label="True")
    plt.scatter(prediction[:,0], prediction[:,1], label="Predicted")
    plt.text(0.05, 0.95, f"RMSE: {rmse}", transform=plt.gca().transAxes)
    plt.legend()
    plt.savefig(plot_folder+"test_predictions"+suffix+".png")
    #plt.show()
    plt.close()

def plot_image(img, label, prediction, title):
    # print(f"{label=}, {prediction=}")
    xl = label[0]
    yl = label[1]
    xp = prediction[0]
    yp = prediction[1]
    _, ax = plt.subplots(figsize=(8,8))
    plt.imshow(img)
    plt.title(f"True: ({xl:.2f}, {yl:.2f})\nPredicted: ({xp:.2f}, {yp:.2f})")
    plt.plot(xl,yl,f'ko',markersize=3)
    plt.plot(xp,yp,'o', markeredgecolor='red', markeredgewidth=1, markerfacecolor='none', markersize=3)
    plt.plot
    plt.draw()
    plt.savefig(f'{plot_folder}{title}.jpg')
    plt.close()


validation_plots()
# Make prediction
prediction = model.predict(test_images)
# Save predictions
np.save(f'{data_folder}test_predictions{suffix}', prediction)

# Inverse transform
prediction = label_scaler.inverse_transform(prediction).astype(int)
labels = label_scaler.inverse_transform(test_labels).astype(int)
plot_prediction(prediction, labels)

for i in range(len(labels)):
    plot_image(test_images[i], labels[i], prediction[i], title=f"True_{i}-Predicted_{i}{suffix}")

print('Done!')