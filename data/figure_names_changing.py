import os
import glob

# Define the folder containing the files to rename
folder_path = "/mnt/project_mnt/atlas/atlas_gen_fs/vannolil/triplets_cnn/data/dummies/"

# Get a list of all the files in the folder
files = glob.glob(os.path.join(folder_path, "*.jpg"))
files = sorted(files, key=str.lower)
# Define a counter to keep track of the number
counter = 1

# Loop over the files and rename them
for file_path in files:
    # Get the file extension
    _, ext = os.path.splitext(file_path)
    print(file_path)
    # print(f"dummy_cross_center_coords_{counter:02d}{ext}")
    # print(os.path.join(folder_path,f"dummy_cross_center_coords_{counter:02d}{ext}") )
    # Construct the new file name
    new_name = f"dummy_{counter:02d}{ext}"
    
    # Rename the file
    os.rename(file_path, os.path.join(folder_path, new_name))
    
    # Increment the counter
    counter += 1
