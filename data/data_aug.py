import os
import imutils
import cv2
import numpy as np
from os.path import isfile, join
from time import time
from PIL import Image
import glob
import matplotlib.pyplot as plt
import random
from random import seed
import re
from sklearn.preprocessing import MinMaxScaler
import pickle as pkl

#adding noise to image
from random import seed

test = False

img_dir = '/mnt/project_mnt/atlas/atlas_gen_fs/vannolil/cnn_triplets/data/figures/'
label_dir = '/mnt/project_mnt/atlas/atlas_gen_fs/vannolil/cnn_triplets/data/labels/'

pattern = re.compile(r"\d+")
def sort_key(s):
    match = pattern.search(s)
    if match:
        return int(match.group())
    else:
        return float("inf")

def get_image(img_directory=img_dir, label_directory=label_dir):
    input_pic = glob.glob(img_directory+'*.jpg')
    input_pic = sorted(input_pic, key=sort_key)
    label = glob.glob(label_directory+'*.npy')
    label = sorted(label, key=sort_key)
    if test:
        input_pic = [input_pic[0]]
        label = [label[0]]
        print(f"input_pic = {input_pic}\nlabel = {label}")
    else: 
        print(f"input_pic.len() = {len(input_pic)}")
    return input_pic, label
    
def plot_image(img, label, title=''):
    plt.ion()
    im = Image.open(img)
    center_coords = np.load(label)
    center_coords = center_coords.reshape(1,2)
    x = center_coords[0][0]
    y = center_coords[0][1]
    _, ax = plt.subplots(figsize=(8,8))
    ax.imshow(im)
    plot_title = img.split('/')[-1][:-4]+' '+title
    plt.title(plot_title)
    plt.plot(x,y,f'rs',markersize=3)
    plt.draw()
    plt.show()

#generate image with noyse
def noisy(noise_typ,image):
    #guassian noise with mean = 0 and sigma = 1.5
    row,col,ch= image.shape
    if noise_typ == "gauss":
        mean = 0.0
        var = 1.2
        sigma = var**0.5
        gauss = np.random.normal(mean,sigma,(row,col,ch))
        gauss = gauss.reshape(row,col,ch)
        noisy = image+ image*gauss
        return noisy
    elif noise_typ == "luminosity_1":
        # Increase the brightness and contrast
        brightness = 50 # increase this value to increase the brightness
        contrast = 30 # increase this value to increase the contrast
        adjusted = cv2.addWeighted(image, 1 + contrast/127, image, 0, brightness - contrast)
        return adjusted
    elif noise_typ == "luminosity_2":
        # Increase the brightness and contrast
        brightness = 100 # increase this value to increase the brightness
        contrast = 0 # increase this value to increase the contrast
        adjusted = cv2.addWeighted(image, 1 + contrast/127, image, 0, brightness - contrast)
        return adjusted
    elif noise_typ == "luminosity_3":
        # Increase the brightness and contrast
        brightness = 0 # increase this value to increase the brightness
        contrast = 0 # increase this value to increase the contrast
        adjusted = cv2.addWeighted(image, 1 + contrast/127, image, 0, brightness - contrast)
        return adjusted
    #red filter
    elif noise_typ == "red":
        red_img  = np.full((row,col,ch), (0,0,255), np.uint8)
        noisy = cv2.addWeighted(image, 0.8, red_img, 0.2, 0)
        return noisy
    #blue filter
    elif noise_typ =='blue':
        blue_img  = np.full((row,col,ch), (255,0,0), np.uint8)
        noisy = cv2.addWeighted(image, 0.8, blue_img, 0.2, 0)
        return noisy
    #green filter
    elif noise_typ =='green':
        green_img  = np.full((row,col,ch), (0,255,0), np.uint8)
        noisy = cv2.addWeighted(image, 0.8, green_img, 0.2, 0)
        return noisy
    #guassian noise with mean = 0 and sigma = 1
    elif noise_typ =="speckle":
        gauss = np.random.randn(row,col,ch)*0.3
        gauss = gauss.reshape(row,col,ch)        
        noisy = image + image * gauss
        return noisy

#generate image with noyse
def image_noise(prob = 1/5, gauss = True, speckle = False, red = True, green = True, blue = True, luminosity_1 = True, luminosity_2 = True, luminosity_3 = True):
    # seed random number generator
    seed(1)
    print('image noysing is running')
    images, labels = get_image(img_dir, label_dir)
    print('start:')
    print('input_pic len: {}'.format(len(images)))
    print('label_pic len: {}'.format(len(labels)))
    start = len(images)+1
    for i,(img,label) in enumerate(zip(images, labels)):
        #value = random()
        image = cv2.imread(img)
        center_coords = np.load(label)
        image_name_no_number = ''.join([f for f in img if not f.isdigit()]).split("/")[-1]
        label_name_no_number = ''.join([f for f in label if not f.isdigit()]).split("/")[-1]
        if gauss: #and value <= prob:
            img_noysed = noisy("gauss",image)
            cv2.imwrite(img_dir+image_name_no_number[:-4]+str(start)+'.jpg', img_noysed)
            np.save(label_dir+label_name_no_number[:-4]+str(start), center_coords) 
            start += 1
        if speckle: #and value > prob and value <= 2*prob:
            img_noysed = noisy("speckle",image)
            cv2.imwrite(img_dir+image_name_no_number[:-4]+str(start)+'.jpg', img_noysed)
            np.save(label_dir+label_name_no_number[:-4]+str(start), center_coords) 
            start += 1
        if red: #and value > 2*prob and value <= 3*prob:
            img_noysed = noisy("red",image)
            cv2.imwrite(img_dir+image_name_no_number[:-4]+str(start)+'.jpg', img_noysed)
            np.save(label_dir+label_name_no_number[:-4]+str(start), center_coords) 
            start += 1
        if blue: #and value > 3*prob and value <= 4*prob:
            img_noysed = noisy("blue",image)
            cv2.imwrite(img_dir+image_name_no_number[:-4]+str(start)+'.jpg', img_noysed)
            np.save(label_dir+label_name_no_number[:-4]+str(start), center_coords) 
            start += 1
        if green:# and value > 4*prob:
            img_noysed = noisy("green",image)
            cv2.imwrite(img_dir+image_name_no_number[:-4]+str(start)+'.jpg', img_noysed)
            np.save(label_dir+label_name_no_number[:-4]+str(start), center_coords) 
            start += 1
        if luminosity_1: 
            img_noysed = noisy("luminosity_1",image)
            cv2.imwrite(img_dir+image_name_no_number[:-4]+str(start)+'.jpg', img_noysed)
            np.save(label_dir+label_name_no_number[:-4]+str(start), center_coords) 
            start += 1
        if luminosity_2:
            img_noysed = noisy("luminosity_2",image)
            cv2.imwrite(img_dir+image_name_no_number[:-4]+str(start)+'.jpg', img_noysed)
            np.save(label_dir+label_name_no_number[:-4]+str(start), center_coords) 
            start += 1
        if luminosity_3:
            img_noysed = noisy("luminosity_3",image)
            cv2.imwrite(img_dir+image_name_no_number[:-4]+str(start)+'.jpg', img_noysed)
            np.save(label_dir+label_name_no_number[:-4]+str(start), center_coords) 
            start += 1
        if(start%1000==0):
            print("showing the plot of the image")
            print(img_dir+image_name_no_number[:-4]+str(start-1)+'.jpg')
            print(label_dir+label_name_no_number[:-4]+str(start-1)+'.npy')
            plot_image(img_dir+image_name_no_number[:-4]+str(start-1)+'.jpg', label_dir+label_name_no_number[:-4]+str(start-1)+'.npy')
    new_input_pic, new_label_pic = get_image(img_dir, label_dir)
    print('end:')
    print('number of images: input = {}, label = {}'.format(len(new_input_pic), len(new_label_pic)))

#new image and coordinate rotations
def image_rotation(alpha_range = 36):
    print('image rotation is running')
    img, label = get_image(img_dir, label_dir)
    #print(f'{img=}')
    alpha_factor = round(360/int(alpha_range),3)
    num = 0
    print('start:')
    print('input_pic len: {}'.format(len(img)))
    print('label_pic len: {}'.format(len(label)))
    for i, (im, lab) in enumerate(zip(img, label)):
        print(f"image = {im}\nlable = {lab}")
        start = len(img)+num+1
        for alpha in range(int(alpha_range)):
            #print(f"alpha = {alpha}")
            image_name_no_number = ''.join([f for f in im if not f.isdigit()]).split("/")[-1]
            label_name_no_number = ''.join([f for f in lab if not f.isdigit()]).split("/")[-1]
            image = cv2.imread(im)
            coordinates = np.load(lab)
            h, w = image.shape[:2]
            #print(f"image shape = {h}, {w}")
            center = np.array([int(w/2), int(h/2)])
            #print(f"center = {center}")
            _alpha = alpha*alpha_factor
            rotated_image = imutils.rotate(image, _alpha)
            # translate vector so that center is at the origin. The rotation is inverted, so i put a minus on the degree
            theta = np.deg2rad(-_alpha)
            cos_theta = np.cos(theta)
            sin_theta = np.sin(theta)
            rotation_matrix = np.array([[cos_theta, -sin_theta], [sin_theta, cos_theta]])
            rotated_coordinates = np.array([rotation_matrix[0][0]*(coordinates[0]-center[0])+rotation_matrix[0][1]*(coordinates[1]-center[1])+center[0],
                                            rotation_matrix[1][0]*(coordinates[0]-center[0])+rotation_matrix[1][1]*(coordinates[1]-center[1])+center[1]])
            #print(f"coordinates = {coordinates}")
            rotated_coordinates = rotated_coordinates.astype(int)
            #print(f"rotated_coordinantes = {rotated_coordinates}")
            if (rotated_coordinates[0]>0.0 and rotated_coordinates[0]<500.0 and rotated_coordinates[1]>0.0 and rotated_coordinates[1]<500.0):
                cv2.imwrite(img_dir+image_name_no_number[:-4]+str(int(start+alpha))+'.jpg', rotated_image)
                np.save(label_dir+label_name_no_number[:-4]+str(int(start+alpha)), rotated_coordinates) 
                if (start+alpha)%1000==0:
                    print("showing the plot of the image")
                    plot_image(img_dir+image_name_no_number[:-4]+str(int(start+alpha))+'.jpg', label_dir+label_name_no_number[:-4]+str(int(start+alpha))+'.npy', title='rotated')
                num+= 1
    new_input_pic, new_label_pic = get_image(img_dir, label_dir)
    print('end:')
    print('number of images: input = {}, label = {}'.format(len(new_input_pic), len(new_label_pic)))

#flip the image and the coordinates
def image_flip():
    print('image flip is running')
    img, label = get_image(img_dir, label_dir)
    print('start:')
    print('input_pic len: {}'.format(len(img)))
    print('label_pic len: {}'.format(len(label)))
    for i, (im, lab) in enumerate(zip(img, label)):
        start = len(img)+i+1
        image_name_no_number = ''.join([f for f in im if not f.isdigit()]).split("/")[-1]
        label_name_no_number = ''.join([f for f in lab if not f.isdigit()]).split("/")[-1]
        # Mirror the image horizontally
        img = cv2.imread(im)
        coordinates = np.load(lab)
        mirrored_img = cv2.flip(img, 1)
        # Mirror the x-coordinate of the coordinates to match the mirror image
        mirrored_coordinates = np.array([img.shape[1] - coordinates[0], coordinates[1]])
        cv2.imwrite(img_dir+image_name_no_number[:-4]+str(start)+'.jpg', mirrored_img)
        np.save(label_dir+label_name_no_number[:-4]+str(start), mirrored_coordinates)
        if start%1000==0:
            print("showing the plot of the image")
            plot_image(img_dir+image_name_no_number[:-4]+str(int(start))+'.jpg', label_dir+label_name_no_number[:-4]+str(int(start))+'.npy', title='flip')
    new_input_pic, new_label_pic = get_image(img_dir, label_dir)
    print('end:')
    print('number of images: input = {}, label = {}'.format(len(new_input_pic), len(new_label_pic)))

if __name__ == '__main__':
    #image_rotation()
    #image_flip()
    #image_noise()
    images, labls = get_image(img_dir, label_dir)
    imgs= [] 
    labels = []
    max_images = 10000
    random_numbers = random.sample(range(1, len(images)), max_images)
    for i in random_numbers:
        imgs.append(images[i])
        labels.append(labls[i])
    print(f'{len(imgs)=}\n{len(labels)=}')
    if not test:
        i = []
        l = []
        for image, label in zip(imgs, labels):
            img = np.array(Image.open(image), dtype=np.float32)
            lbl = np.load(label).reshape(2)
            i.append(img)
            l.append(lbl)
        i = np.array(i)
        l = np.array(l)
        # Convert lists to arrays
        print(f"{i.shape=}")
        print(f"{l.shape=}")
        input_shape = i.shape
        i = i.reshape((-1, 3))
        # Create scaler object and fit to data
        scaler_images = MinMaxScaler()
        scaler_images.fit(i)
        # Transform data using scaler
        i = scaler_images.transform(i)
        i = i.reshape(input_shape)
        scaler_labels = MinMaxScaler()
        l = scaler_labels.fit_transform(l)

        with open('/mnt/project_mnt/atlas/atlas_gen_fs/vannolil/cnn_triplets/data/image_scaler.pkl', 'wb') as f:
            pkl.dump(scaler_images, f)
        with open('/mnt/project_mnt/atlas/atlas_gen_fs/vannolil/cnn_triplets/data/label_scaler.pkl', 'wb') as f:
            pkl.dump(scaler_labels, f)
        np.save('/mnt/project_mnt/atlas/atlas_gen_fs/vannolil/cnn_triplets/data/img', i)
        np.save('/mnt/project_mnt/atlas/atlas_gen_fs/vannolil/cnn_triplets/data/label', l)
    input("Press Enter to continue...")